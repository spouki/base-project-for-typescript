BASE FOR TYPESCRIPT PROJECT
----
0.0.1
----
The goal of this repo is to provide a quick bootstrapped project 
for using typescript.
It provides a code watcher, compiler and server.

There is a src folder in which you add your code.
The starting file is index.ts.

If you want to add a TS file to src, you need to add the declaration of the file in
the webpack.config.js (located at the root of the project), in the module.export.entry part with the following shape
name: "(./)?path/to/file.ts".

The two command of this project are :

-> start
: starts the server

-> build
: launch the code watcher and compiler

No tests are implemented yet.

No html moving process implemented in the builder yet.


